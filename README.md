usage example:
```
deploy:
  image:
    name: bitnami/kubectl:latest
    entrypoint: ['']
  script:
    - kubectl config use-context kotahi/agent:agent-1
    - kubectl get pods
```
